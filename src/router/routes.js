
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'homepage',
        component: () => import('pages/landing_pages/Index.vue')
      },
      {
        path: '/about-us',
        name: 'aboutUs',
        component: () => import('pages/landing_pages/AboutUs.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes

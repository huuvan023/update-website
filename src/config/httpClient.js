import axios from 'axios'
import * as notifyInstance from './../boot/notify';
import store from './../store/index';
import {USER_NOT_AUTHEN_CODE} from "boot/constant";

const client = axios.create({
    baseURL: process.env.API_URL
});
function setToken() {
  const token = localStorage.getItem(process.env.TOKEN_NAME);
  if (token)
    client.defaults.headers.common['Token-Key'] = token
}

function checkUserNotAuthen(response) {
  if(response.code === USER_NOT_AUTHEN_CODE) {
    store().commit('user/setUser', null)
    store().commit('user/setLoginStatus', false)
    localStorage.removeItem(process.env.TOKEN_NAME)
    notifyInstance.fail(response.message)
    window.location.href = "https://forum.poicollector.com/login?message=1";
  }
}
async function get(url) {
    try {
      await setToken();
        const response = await client.get(url)
      await checkUserNotAuthen(response.data.result)
        if (response.status === 200) {
            return response.data
        }
    } catch (error) {
        throw error
    }
}

async function post(url, data, options = {}) {
    try {
      await setToken();
        const response = await client.post(url, data, options)
      await checkUserNotAuthen(response.data.result)
        if (response.status === 200) {
            return response.data
        }
    } catch (error) {
        throw error
    }
}

async function put(url, data) {
    try {
      await setToken();
        const response = await client.put(url, data)
      await checkUserNotAuthen(response.data.result)
        if (response.status === 200) {
            return response.data
        }
    } catch (error) {
        throw error
    }
}

async function del(url, data) {
    try {
      await setToken();
        const response = await client.delete(url, {data})
      await checkUserNotAuthen(response.data.result)
        if (response.status === 200) {
            return response.data
        }
    } catch (error) {
        throw error
    }
}

export const httpClient = {
    get,
    post,
    put,
    del
};
